from django.urls import path

import projects.views as views

urlpatterns = [
    # login
    path('details/', views.details),
    path('change/', views.change),
    path('page/', views.page),
]
