#!/usr/bin/env python
# -*- coding: utf-8 -*-
# custom admin page
from django.contrib import admin

from polls.models import Question, Choice


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 4


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    inlines = (ChoiceInline,)
    fields = ('question_text', 'enable')
    list_display = ('question_text', 'enable', 'create_time', 'update_time')
    ordering = ('-update_time', )
