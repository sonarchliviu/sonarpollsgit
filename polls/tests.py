from django.test import TestCase

from django.urls import reverse

class PollsTestCase(TestCase):

    def test_index(self):
        url = reverse('index')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 302)
